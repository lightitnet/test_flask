from flask_login import LoginManager


class ApplicationManager:
    login_manager = None

    @classmethod
    def register_applications(cls, app):
        from profile import profile_blueprint
        from weather import weather_blueprint

        app.register_blueprint(weather_blueprint)
        app.register_blueprint(profile_blueprint)

    @classmethod
    def init_login_manager(cls, app):
        cls.login_manager = LoginManager(app)
        cls.login_manager.login_view = "profile.login"
