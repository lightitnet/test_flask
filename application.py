#!/usr/bin/env python
import settings
from application_manager import ApplicationManager

from flask import Flask
from flask_script import Manager, Server
from flask_migrate import Migrate, MigrateCommand
from database_manager import DatabaseManager

from commands import GenerateFakeData


def create_app(app_name):
    app = Flask(
        app_name,
        template_folder=settings.TEMPLATES_FOLDER,
        static_folder=settings.STATIC_FOLDER,
    )
    app.debug = settings.DEBUG
    app.secret_key = settings.SECRET_KEY
    app.config.from_object(settings.ConfigApp)
    return app


app = create_app('Weather')

db = DatabaseManager.setup_db(app)

migrate = Migrate(app, db)

# Load login manager
ApplicationManager.init_login_manager(app=app)

# Load blueprints
ApplicationManager.register_applications(app=app)

manager = Manager(app)
manager.add_command('db', MigrateCommand)
manager.add_command('runserver', Server(
    host=settings.HOST, port=settings.PORT))
manager.add_command('generate_fake_data', GenerateFakeData(db))

if __name__ == '__main__':
    manager.run()
