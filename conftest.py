from pytest import fixture
from application import create_app, db as _db
from application_manager import ApplicationManager

SQLALCHEMY_DATABASE_URI = 'postgresql://test_weather:123qwe@localhost:5432/test_base_flask'


@fixture
def app():
    app = create_app('Weather Test')
    app.testing = True
    app.debug = True
    ApplicationManager.init_login_manager(app=app)
    ApplicationManager.register_applications(app=app)
    app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
    yield app


@fixture
def db(app):
    _db.app = app
    _db.init_app(app)
    with app.app_context():
        _db.create_all(app=app)
        yield _db
        _db.session.remove()
        _db.drop_all()
