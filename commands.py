import random
from datetime import datetime, timedelta

from flask_script import Command


class GenerateFakeData(Command):
    WINTER_MONTHS = [12, 1, 2]
    SUMMER_MONTHS = [6, 7, 8]

    def __init__(self, db):
        self.db = db
        super().__init__()

    def run(self, months=10):
        from weather.models.weather import WeatherInfo

        date_to_proceed = datetime.now()
        date_range = months * 4 * 30
        data_to_db = []
        temperature = 0
        for i in range(date_range):
            date_to_proceed -= timedelta(days=1)
            if date_to_proceed.month in self.WINTER_MONTHS:
                temperature = random.randrange(-25, 5, 1)
            elif date_to_proceed.month in self.SUMMER_MONTHS:
                temperature = random.randrange(20, 42, 1)
            else:
                temperature = random.randrange(5, 20, 1)
            data_to_db.append(
                WeatherInfo(temperature=temperature, date=date_to_proceed))
        self.db.session.bulk_save_objects(data_to_db)
        self.db.session.commit()
