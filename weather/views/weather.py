from flask import render_template, request, jsonify
from flask.views import MethodView
from flask_login import login_required
from sqlalchemy import extract

from weather.models.weather import WeatherInfo


class WeatherView(MethodView):
    @staticmethod
    @login_required
    def get():
        return render_template('weather/main.html')

    @staticmethod
    @login_required
    def post():
        year = request.form['year']
        month = request.form['month']
        data = WeatherInfo.query.order_by(WeatherInfo.date).filter(
            extract('year', WeatherInfo.date) == year,
            extract('month', WeatherInfo.date) == month).all()
        return jsonify(list(w_i.serialize() for w_i in data))
