from database_manager import DatabaseManager


db = DatabaseManager.db


class WeatherInfo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, nullable=False)
    temperature = db.Column(db.Float)

    def serialize(self):
        return {
            'weather_info_id': self.id,
            'date': self.date.strftime("%d-%m-%Y"),
            'temperature': self.temperature,
        }
