from weather.views.weather import WeatherView


def register_routes(blueprint):
    blueprint.add_url_rule('/', view_func=WeatherView.as_view('main'))
