import pytest
from flask import url_for

from profile.models.user import User


@pytest.mark.options(debug=False)
def test_app(app):
    assert not app.debug, 'Ensure the app not in debug mode'


class TestPages:
    def test_login_page(self, client):
        assert client.get(url_for('profile.login')).status_code == 200

    def test_weather_page_unauthorized(self, client):
        assert client.get(url_for('weather.main')).status_code == 302, 'Unauthorized get'

    def test_db_user(self, db):
        user = User(username='test', password='test')
        db.session.add(user)
        db.session.commit()
        assert User.query.count() > 0
        assert db.session.query(
            User.query.filter(User.username == 'test').exists()
        ).scalar()
