from flask import render_template, flash, redirect, url_for
from flask.views import MethodView
from flask_login import login_user, current_user

from profile.forms import LoginForm
from profile.models import User


class LoginView(MethodView):
    @staticmethod
    def get():
        if current_user.is_authenticated:
            return redirect(url_for('weather.main'))
        form = LoginForm()
        return render_template('profile/login.html', form=form)

    @staticmethod
    def post():
        form = LoginForm()
        error_msg = "Wrong password or username"

        if form.validate_on_submit():
            user = User.query.filter_by(username=form.username.data).first()
            if user:
                if user.password == form.password.data:
                    login_user(user)
                    flash("Logged in!", "success")
                    return redirect(url_for('weather.main'))
                else:
                    flash(error_msg, "danger")
            else:
                flash(error_msg, "danger")
        return render_template('profile/login.html', form=form)
