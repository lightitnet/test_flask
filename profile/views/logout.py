from flask import redirect, url_for
from flask.views import MethodView
from flask_login import logout_user, login_required


class LogoutView(MethodView):
    @staticmethod
    @login_required
    def get():
        logout_user()
        return redirect(url_for('profile.login'))
