from flask import render_template, flash, redirect, url_for
from flask.views import MethodView

from profile.forms import RegistrationForm
from profile.models import User


class RegistrationView(MethodView):
    @staticmethod
    def get():
        form = RegistrationForm()
        return render_template('profile/registration.html', form=form)

    @staticmethod
    def post():
        form = RegistrationForm()
        if form.validate_on_submit():
            user = User.query.filter_by(username=form.username.data).first()
            if user:
                flash("User already exist!", "danger")
                return redirect(url_for('profile.registration'))
            user = User(username=form.username.data,
                        password=form.password.data)
            user.save()
            flash("User successfully created.", "success")
            return redirect(url_for('profile.login'))
        return render_template('profile/registration.html', form=form)
